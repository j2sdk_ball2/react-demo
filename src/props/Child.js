import React, { Component } from 'react'
import { render } from 'react-dom'

class Child extends Component {
  render() {
    let { title, onChange } = this.props
    return (
      <div>
        <div>
          <input
            type="submit"
            value="Change Props"
            onClick={() => onChange((title === 'Child') ? 'Parent' : 'Child')}
          />
        </div>
        <div>
          Child: {title}
        </div>
      </div>
    )
  }
}

export default Child