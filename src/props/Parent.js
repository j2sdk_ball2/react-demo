import React, { Component } from 'react';
import { render } from 'react-dom'
import Child from './Child';

class Parent extends Component {
  state = {
    title: 'Parent',
  }

  handleChange = value => {
    this.setState({ title: value })
  }

  render() {
    const { title } = this.state

    return (
      <div>
        <Child
          title={title}
          onChange={value => this.handleChange(value)}
        />
        Parent: {title}
      </div>
    );
  }
}

export default Parent;