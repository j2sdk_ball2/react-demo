import React, { Component } from 'react'
import { render } from 'react-dom'
import Parent from './props/Parent';

class Root extends Component {
  state = {
    keyword: '',
  }

  handleChange = e => {
    const { target: { name, value } } = e
    console.log(name, value)
    this.setState({ [name]: value })
  }

  render() {
    const { keyword } = this.state

    return (
      <div>
        <div>Hello your first React app.</div>
        <hr />
        <div>Binding example:</div>
        <div>
          <input
            id="keyword"
            name="keyword"
            type="text"
            value={keyword}
            onChange={e => this.handleChange(e)}
          />
        </div>
        <br />
        <div>{keyword}</div>
        <Parent />
      </div>
    )
  }
}

render(<Root />, document.querySelector('react'))
