const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const resolve = path.resolve;
const sourcePath = resolve(__dirname, 'src');

module.exports = {
  context: sourcePath,
  entry: [
    './App',
  ],
  output: {
    filename: 'bundle.js',
    path: resolve(__dirname, 'dist'),
  },
  // devServer: {
  //   host: '0.0.0.0',
  //   port: 8082,
  // },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        loader: 'babel-loader',
      },
    ],
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: resolve(__dirname + '/index.html'),
      filename: 'index.html',
    }),
  ],
};
